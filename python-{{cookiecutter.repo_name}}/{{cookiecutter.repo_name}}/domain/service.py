from typing import List

from {{cookiecutter.repo_name}}.domain.command import Command
from {{cookiecutter.repo_name}}.domain.event import Event


class Service:
    """Abstract base class for domain services"""

    def execute(self, command: Command, **kwargs) -> List[Event]:
        raise NotImplementedError
