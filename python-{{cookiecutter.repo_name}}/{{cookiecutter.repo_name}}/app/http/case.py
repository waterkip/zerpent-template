from flask import Blueprint

from {{cookiecutter.repo_name}}.app.asl import dispatch
from {{cookiecutter.repo_name}}.domain.case.commands import TransitionCaseCommand
from {{cookiecutter.repo_name}}.domain.case.service import Service
from {{cookiecutter.repo_name}}.util import build_v1_response

case = Blueprint("case", __name__)


@case.route("/<uuid:case_id>/transition")
def transition(case_id):
    command = TransitionCaseCommand(case_id)
    reference_id = dispatch(command, Service())

    return build_v1_response(
        {"type": "command", "reference": reference_id, "instance": None}
    )
