"""

This module provides a collection of actions for the project.

See :ref:`action` for more information on the concept of what an action is.

"""
