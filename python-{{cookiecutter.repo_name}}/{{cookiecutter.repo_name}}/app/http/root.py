from flask import Blueprint, url_for

import {{cookiecutter.repo_name}}
from {{cookiecutter.repo_name}}.util import build_v1_response

root = Blueprint("root", __name__)


@root.route("/", methods=["GET"])
def service_definition():
    return build_v1_response(
        {
            "type": "service",
            "reference": None,
            "instance": {
                "name": "Zerpent demo application",
                "repository": "https://gitlab.com/zaaksysteem/{{cookiecutter.repo_name}}",
                "version": {{cookiecutter.repo_name}}.__version__,
                "documentation": url_for("docs.doc_page", _external=True),
            },
        }
    )
