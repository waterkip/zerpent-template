"""
{{ cookiecutter.project_short_description }}

.. moduleauthor:: {{cookiecutter.full_name}} <{{cookiecutter.email}}>

"""

__version__ = "{{cookiecutter.version}}"

{%- if cookiecutter.type == 'http' %}
import flask_statsdclient
import logging
import sys
import json_logging

from decouple import config
from flask import Flask
from {{cookiecutter.repo_name}}.app.http.case import case
from {{cookiecutter.repo_name}}.app.http.error_handlers import endpoint_not_found
from {{cookiecutter.repo_name}}.app.http.error_handlers import internal_server_error
from {{cookiecutter.repo_name}}.app.http.root import root
from {{cookiecutter.repo_name}}.docs import docs

app = Flask(__name__)

OTAP_STATE = config("OTAP", default="development")

if OTAP_STATE != "development":
    json_logging.ENABLE_JSON_LOGGING = True
    json_logging.init(framework_name="flask")
    json_logging.init_request_instrument(app)

    statsd = flask_statsdclient.StatsDClient()
    statsd.init_app(app)

logger = logging.getLogger("flask.app")
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

app.register_blueprint(root, url_prefix="/")
app.register_blueprint(docs, url_prefix="/docs")
app.register_blueprint(case, url_prefix="/case")

app.register_error_handler(404, endpoint_not_found)
app.register_error_handler(500, internal_server_error)
{%- elif cookiecutter.type == 'rmq' %}
# RMQ __init__.py
{%- else %}
# python bare-ish __init__.py
{%- endif %}
