from uuid import uuid4

from {{cookiecutter.repo_name}}.domain.command import Command


class Journal:
    def __init__(self, app=None):
        self.entries = []

    def add_entry(self, command: Command):
        self.entries.append(command)
        return uuid4()

    def flush():
        pass
