from typing import Dict

from decouple import config
from flask import Response
from flask.json import jsonify


def build_v1_response(data: Dict, **response_args) -> Response:
    """Builds a Zaaksysteem APIv1-style HTTP response, given result data"""
    otap = config("OTAP", default="development")

    status_code = response_args.get("status", 200)

    response = jsonify(
        {
            "request_id": None,
            "development": True if otap == "development" else False,
            "api_version": 1,
            "status_code": status_code,
            "result": data,
        }
    )

    response.status_code = status_code

    return response
