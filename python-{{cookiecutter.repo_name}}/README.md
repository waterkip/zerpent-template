# Zerpent

Zaaksysteem template project for Python (micro)services

## Getting started

n/a

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/zaaksysteem/{{cookiecutter.repo_name}}/blob/master/CONTRIBUTING.md)
for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](https://semver.org/) for versioning. For the versions
available, see the
[tags on this repository](https://gitlab.com/zaaksysteem/{{cookiecutter.repo_name}}/tags/).

## License

Copyright (c) 2018, Mintlab B.V. and all persons listed in the
[CONTRIBUTORS.md](https://gitlab.com/zaaksysteem/{{cookiecutter.repo_name}}/blob/master/CONTRIBUTORS.md)
file.

This project is licensed under the EUPL, v1.2. See the
[EUPL-1.2.txt](https://gitlab.com/zaaksysteem/{{cookiecutter.repo_name}}/blob/master/EUPL-1.2.txt)
file for details.

## Acknowledgements

* Gemeente Bussum (https://www.bussum.nl/)

  For bootstrapping the Zaaksysteem project and their support in making it
  into the first of its kind open-source project for municipalities of
  The Netherlands.
